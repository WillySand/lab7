from django.test import TestCase,Client
from django.urls import resolve
from .views import home
from django.apps import apps
from .apps import Story7Config
   

class Lab7UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'Story7')
        self.assertEqual(apps.get_app_config('Story7').name, 'Story7')
    def test_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)#test existing url
        response = Client().get('/admin')
        self.assertNotEqual(response.status_code, 200)#test url that doesn't exist
        response = Client().get('/home')
        self.assertNotEqual(response.status_code, 200)
    def test_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, home)
